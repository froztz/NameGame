package com.willowtreeapps.namegame.util;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

/**
 * Created by Frostz on 9/20/2017.
 */

public class RandomImage implements Parcelable {

    private int position;
    private ImageView imageView;

    public RandomImage(int position, ImageView imageView) {
        this.position = position;
        this.imageView = imageView;
    }

    public int getPosition() {
        return position;
    }

    public ImageView getImageView() {
        return imageView;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.position);
        dest.writeParcelable((Parcelable) this.imageView, flags);
    }

    protected RandomImage(Parcel in) {
        this.position = in.readInt();
        this.imageView = in.readParcelable(ImageView.class.getClassLoader());
    }

    public static final Creator<RandomImage> CREATOR = new Creator<RandomImage>() {
        @Override
        public RandomImage createFromParcel(Parcel source) {
            return new RandomImage(source);
        }

        @Override
        public RandomImage[] newArray(int size) {
            return new RandomImage[size];
        }
    };
}
