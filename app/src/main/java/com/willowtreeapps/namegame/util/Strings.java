package com.willowtreeapps.namegame.util;

/**
 * Created by Frostz on 9/20/2017.
 */

public class Strings {
    public static final String FRAG_TAG = "NameGameFragmentTag";
    public static final String ADDED_SETUP = "addedSetup";
    public static final String SETUP_NAME = "addedSetup";
    public static final String SETUP = "setup";
    public static final String GAME_NAME = "game";

    // Bundle Keys
    public static final String BUNDLE_PROFILES = "profiles";
    public static final String BUNDLE_MODE = "mode";
    public static final String BUNDLE_PERSONS = "persons";
    public static final String BUNDLE_TARGET = "targetName";
    public static final String BUNDLE_PICKS = "picks";
    public static final String BUNDLE_FOUND = "hasFound";
    public static final String BUNDLE_RANDOM = "random";
}
