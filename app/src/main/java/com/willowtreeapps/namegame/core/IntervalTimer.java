package com.willowtreeapps.namegame.core;


import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Frostz on 9/19/2017.
 */

public final class IntervalTimer {

    public Observable getTimer(){
        return Observable.interval(5000, 3000, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread()).cache();
    }
}
