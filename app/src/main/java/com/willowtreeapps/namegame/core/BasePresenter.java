package com.willowtreeapps.namegame.core;

/**
 * Created by Frostz on 9/17/2017.
 */

public interface BasePresenter<T> {
    void bindView(T view);

    void unbindView();
}
