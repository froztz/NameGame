package com.willowtreeapps.namegame.ui.mvp;

import com.willowtreeapps.namegame.core.IntervalTimer;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Frostz on 9/19/2017.
 */

@Module
public class PresenterModule {

    @Provides
    NameGameContract.Presenter provideNameGamePresenter(IntervalTimer intervalTimer){
        return new NameGamePresenter(intervalTimer);
    }
}
