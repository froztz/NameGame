package com.willowtreeapps.namegame.ui;

import android.content.Context;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.willowtreeapps.namegame.R;
import com.willowtreeapps.namegame.core.NameGameApplication;
import com.willowtreeapps.namegame.network.api.ProfilesRepository;
import com.willowtreeapps.namegame.network.api.model.Person;
import com.willowtreeapps.namegame.ui.mvp.NameGameContract;
import com.willowtreeapps.namegame.util.CircleBorderTransform;
import com.willowtreeapps.namegame.util.RandomImage;
import com.willowtreeapps.namegame.util.Strings;
import com.willowtreeapps.namegame.util.Ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;


public class NameGameFragment extends Fragment implements NameGameContract.View{

    private static final Interpolator OVERSHOOT = new OvershootInterpolator();

    @Inject
    Picasso picasso;
    @Inject
    ProfilesRepository repository;
    @Inject
    NameGameContract.Presenter presenter;

    private TextView title;
    private ViewGroup container;
    private List<ImageView> faces = new ArrayList<>(6);
    private OnFragmentInteractionListener mListener;
    private LightingColorFilter correctFilter = new LightingColorFilter(0xFF00AA00, 0x00000000);
    private LightingColorFilter wrongFilter = new LightingColorFilter(0xFFAA0000, 0x00000000);
    private List<RandomImage> random;

    @Inject
    public NameGameFragment(){}


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NameGameApplication.get(getActivity()).component().inject(this);
        if (savedInstanceState != null) {
            presenter.restoreInstanceState(savedInstanceState.getParcelableArrayList(Strings.BUNDLE_PERSONS),
                    savedInstanceState.getString(Strings.BUNDLE_TARGET),
                    savedInstanceState.getBooleanArray(Strings.BUNDLE_PICKS),
                    savedInstanceState.getBoolean(Strings.BUNDLE_FOUND),
                    savedInstanceState.getBoolean(Strings.BUNDLE_MODE));
            random = savedInstanceState.getParcelable(Strings.BUNDLE_RANDOM);
        } else if (getArguments() != null) {
            ArrayList<Person> persons = getArguments().getParcelableArrayList(Strings.BUNDLE_PROFILES);
            presenter.onCreate(persons, getArguments().getBoolean(Strings.BUNDLE_MODE));
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        presenter.bindView(this);
        return inflater.inflate(R.layout.name_game_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        title = (TextView) view.findViewById(R.id.title);
        container = (ViewGroup) view.findViewById(R.id.face_container);
        presenter.onViewReady();
    }

    public void setupView(ArrayList<Person> persons, Boolean hasFound, String targetName) {

            //Hide the views until data loads
            if (!hasFound) {
                title.setText(getResources().getString(R.string.question, targetName));
            } else {
                title.setText(random == null ? getResources().getString(R.string.last_choice, presenter.getTargetName()): getResources().getString(R.string.correct_choice));
                title.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListener.onCorrectInteraction();
                    }
                });
            }
            title.setAlpha(0);

            int n = container.getChildCount();
            for (int i = 0; i < n; i++) {
                ImageView face = (ImageView) container.getChildAt(i);
                faces.add(face);

                //Hide the views until data loads
                face.setScaleX(0);
                face.setScaleY(0);
            }
            presenter.setImages();
            presenter.handleIfNormalMode();
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.unbindView();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    /**
     * A method for setting the images from people into the imageviews
     */
    public void setImages(final ArrayList<Person> persons) {
        int imageSize = (int) Ui.convertDpToPixel(100, getContext());
        int n = faces.size();

        for (int i = 0; i < n; i++) {
            ImageView face = faces.get(i);
            final int finalI = i;
            face.setOnClickListener(!presenter.hasFound() ? new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onPersonSelected(v, finalI);
                }
            } : null);
            picasso.load("http:" + persons.get(i).getHeadshot().getUrl())
                    .placeholder(R.drawable.ic_face_white_48dp)
                    .resize(imageSize, imageSize)
                    .transform(new CircleBorderTransform())
                    .into(face);
            animateFacesIn();
        }
    }

    /**
     * A method to animate the faces into view
     */
    public void animateFacesIn() {
        title.animate().alpha(1).start();
        for (int i = 0; i < faces.size(); i++) {
            ImageView face = faces.get(i);
            face.animate().scaleX(1).scaleY(1).setStartDelay(800 + 120 * i).setInterpolator(OVERSHOOT).start();
            if (presenter.hasPicked(i)) {
                if (presenter.animateFaceIn(i) == null){
                    face.setVisibility(View.INVISIBLE);
                } else {
                    face.setColorFilter(presenter.animateFaceIn(i) ? correctFilter: wrongFilter);
                }
            }
        }
    }

    /**
     * A method to handle when a person is selected
     *
     * @param view   The view that was selected
     * @param position The position of the selected view
     */
    private void onPersonSelected(@NonNull View view, @NonNull int position) {
        //TODO evaluate whether it was the right person and make an action based on that
        if (presenter.personSelected(position) == null){
            view.setVisibility(View.INVISIBLE);
        } else {
            ((ImageView) view).setColorFilter(presenter.personSelected(position) ? correctFilter : wrongFilter);
            correct(presenter.personSelected(position));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(Strings.BUNDLE_RANDOM, (ArrayList<? extends Parcelable>) random);
        outState.putParcelableArrayList(Strings.BUNDLE_PERSONS, presenter.getPersons());
        outState.putBooleanArray(Strings.BUNDLE_PICKS, presenter.getPicks());
        outState.putString(Strings.BUNDLE_TARGET, presenter.getTargetName());
        outState.putBoolean(Strings.BUNDLE_FOUND, presenter.hasFound());
        outState.putBoolean(Strings.BUNDLE_MODE, presenter.isNormalMode());
    }

    private void correct(boolean isCorrect) {
        if (isCorrect) {
            presenter.correct();
            removeListeners();
            title.setText(R.string.correct_choice);
            title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onCorrectInteraction();
                }
            });
        }
    }

    private void lastView(){
        presenter.handleLast();
        removeListeners();
        title.setText(getResources().getString(R.string.last_choice, presenter.getTargetName()));
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onCorrectInteraction();
            }
        });
    }

    private void removeListeners(){
        for (View v : faces) {
            v.setOnClickListener(null);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onCorrectInteraction();
    }

    public void removeRandom(){
        if (!random.isEmpty()) {
            Random rand = new Random();
            int i = rand.nextInt(random.size());
            presenter.didPick(random.get(i).getPosition());
            random.get(i).getImageView().setVisibility(View.INVISIBLE);
            random.remove(i);
            if (random.isEmpty()){
                lastView();
            }
        }
    }

    public void setupNonNormal(){
        if (random == null) {
            random = new ArrayList<>();
            for (int i = 0; i < faces.size(); i++){
                if (!presenter.isTarget(i) && !presenter.hasPicked(i)){
                    random.add(new RandomImage(i, faces.get(i)));
                }
            }
        }
    }
}
