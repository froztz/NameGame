package com.willowtreeapps.namegame.ui.mvp;


import com.willowtreeapps.namegame.core.IntervalTimer;
import com.willowtreeapps.namegame.network.api.model.Person;
import com.willowtreeapps.namegame.util.Preconditions;

import java.util.ArrayList;
import java.util.Random;

import javax.inject.Inject;

import rx.Subscription;
import rx.functions.Action1;

/**
 * Created by Frostz on 9/20/2017.
 */

public class NameGamePresenter implements NameGameContract.Presenter {


    private IntervalTimer intervalTimer;
    private NameGameContract.View view;
    private ArrayList<Person> persons;
    private boolean[] picks;
    private boolean hasFound;
    private boolean isNormalMode;
    private String targetName;
    private Subscription subscription;

    @Inject
    public NameGamePresenter(IntervalTimer intervalTimer) {
        this.intervalTimer = intervalTimer;
    }

    @Override
    public void bindView(NameGameContract.View view) {
        this.view = Preconditions.checkNotNull(view, "Presenter can not bind null view.");
    }

    @Override
    public void unbindView() {
        this.view = null;
    }

    @Override
    public void onCreate(ArrayList<Person> persons, boolean isNormalMode) {
        this.persons = Preconditions.checkNotNullOrEmpty(persons, "Persons can not be null or empty.");
        this.isNormalMode = Preconditions.checkNotNull(isNormalMode, "The game mode may not be null.");
        this.targetName = persons.get(new Random().nextInt(persons.size())).getFirstName();
        this.picks = new boolean[6];
    }

    @Override
    public void onViewReady() {
        view.setupView(persons, hasFound, targetName);
    }

    @Override
    public void handleIfNormalMode() {
        if (!isNormalMode && !hasFound) {
            view.setupNonNormal();
            subscription = intervalTimer.getTimer().subscribe(new Action1() {
                @Override
                public void call(Object o) {
                    view.removeRandom();
                }
            });
        }
    }

    @Override
    public void setImages() {
        view.setImages(persons);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onPause() {
        removeSubscriptions();
    }

    private void removeSubscriptions() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    @Override
    public void onResume() {

    }

    @Override
    public void correct() {
        removeSubscriptions();
    }

    @Override
    public boolean hasFound() {
        return hasFound;
    }

    @Override
    public void didPick(int i) {
        picks[i] = true;
    }

    @Override
    public void handleLast() {
        hasFound = true;
        removeSubscriptions();
    }

    @Override
    public boolean hasPicked(int i) {
        return picks[i];
    }

    @Override
    public Boolean animateFaceIn(int i) {
        return isNormalMode || isTarget(i) ? isTarget(i) : null;
    }

    @Override
    public boolean isTarget(int i) {
        return persons.get(i).getFirstName().equals(targetName);
    }

    @Override
    public Boolean personSelected(int i) {
        didPick(i);
        Boolean correct = animateFaceIn(i);
        hasFound = correct != null ? correct : false;
        return correct;
    }

    @Override
    public ArrayList getPersons() {
        return persons;
    }

    @Override
    public boolean[] getPicks() {
        return picks;
    }

    @Override
    public String getTargetName() {
        return targetName;
    }

    @Override
    public Boolean isNormalMode() {
        return isNormalMode;
    }


    @Override
    public void restoreInstanceState(ArrayList persons, String targetName, boolean[] picks, boolean hasFound, boolean isNormalMode) {
        this.persons = persons;
        this.targetName = targetName;
        this.picks = picks;
        this.hasFound = hasFound;
        this.isNormalMode = isNormalMode;
    }
}
