package com.willowtreeapps.namegame.ui;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.willowtreeapps.namegame.R;
import com.willowtreeapps.namegame.core.ListRandomizer;
import com.willowtreeapps.namegame.core.NameGameApplication;
import com.willowtreeapps.namegame.network.api.ProfilesRepository;
import com.willowtreeapps.namegame.network.api.model.Profiles;
import com.willowtreeapps.namegame.util.Strings;

import java.util.ArrayList;

import javax.inject.Inject;

public class NameGameActivity extends AppCompatActivity implements NameGameSetupFragment.OnFragmentInteractionListener, NameGameFragment.OnFragmentInteractionListener {

    @Inject
    ProfilesRepository repository;
    @Inject
    ListRandomizer randomizer;


    private Profiles profiles;
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.name_game_activity);
        fragmentManager = getSupportFragmentManager();
        NameGameApplication.get(this).component().inject(this);
        if (savedInstanceState == null) {
            repository.register(new ProfilesRepository.Listener() {
                @Override
                public void onLoadFinished(@NonNull Profiles people) {
                    profiles = people;
                    handleTransaction();
                }

                @Override
                public void onError(@NonNull Throwable error) {

                }
            });
        }
    }

    private void handleTransaction() {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        NameGameSetupFragment f1 = new NameGameSetupFragment();
        handleTransaction(fragmentTransaction, f1, R.id.container, Strings.ADDED_SETUP, Strings.SETUP, makeBundle(false));
    }

    private Bundle makeBundle(boolean isNormalMode) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(Strings.BUNDLE_PROFILES, (ArrayList<? extends Parcelable>) randomizer.pickN(profiles.getPeople(), 6));
        bundle.putBoolean(Strings.BUNDLE_MODE, isNormalMode);
        return bundle;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(Strings.FRAG_TAG, profiles);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        profiles = savedInstanceState.getParcelable(Strings.FRAG_TAG);
    }

    @Override
    public void onFragmentInteraction(int i) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        NameGameFragment f1 = new NameGameFragment();
        int containerId = R.id.container;
        switch (i) {
            case 0:
                handleTransaction(fragmentTransaction, f1, containerId, Strings.GAME_NAME, Strings.FRAG_TAG, makeBundle(true));
                break;
            case 1:
                handleTransaction(fragmentTransaction, f1, containerId, Strings.GAME_NAME, Strings.FRAG_TAG, makeBundle(false));
                break;
            case 2:
                break;
            default:
                break;
        }
    }

    private void handleTransaction(FragmentTransaction fragmentTransaction, Fragment f1, int containerId, String name, String tag, Bundle args) {
        f1.setArguments(args);
        fragmentTransaction.replace(containerId, f1, tag);
        fragmentTransaction.addToBackStack(name);
        fragmentTransaction.commit();
    }

    @Override
    public void onCorrectInteraction() {
        fragmentManager.popBackStack(Strings.SETUP_NAME, 0);
    }

    @Override
    public void onBackPressed() {
        fragmentManager.popBackStack(Strings.SETUP_NAME, 0);
    }
}
