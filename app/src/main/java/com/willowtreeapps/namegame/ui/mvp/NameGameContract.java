package com.willowtreeapps.namegame.ui.mvp;


import com.willowtreeapps.namegame.core.BasePresenter;
import com.willowtreeapps.namegame.network.api.model.Person;

import java.util.ArrayList;

/**
 * Created by Frostz on 9/17/2017.
 */

public interface NameGameContract {

    interface View {
        void setImages(final ArrayList<Person> persons);

        void animateFacesIn();

        void setupView(ArrayList<Person> persons, Boolean hasFound, String targetName);

        void setupNonNormal();

        void removeRandom();
    }

    interface Presenter extends BasePresenter<View> {
        void onViewReady();

        void onCreate(ArrayList<Person> persons, boolean isNormalMode);

        void handleIfNormalMode();

        void setImages();

        void onStart();

        void onPause();

        void onResume();

        void correct();

        void handleLast();

        Boolean animateFaceIn(int i);

        boolean hasPicked(int i);

        void didPick(int i);

        boolean hasFound();

        boolean isTarget(int i);

        Boolean personSelected(int i);

        ArrayList getPersons();

        boolean[] getPicks();

        String getTargetName();

        Boolean isNormalMode();

        void restoreInstanceState(ArrayList persons, String targetName, boolean[] picks, boolean hasFound, boolean isNormalMode);
    }
}
